const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 4001;

mongoose.connect(`mongodb+srv://aronndc:admin123@zuitt-batch-197.lzwxz7u.mongodb.net/S35?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connection to MongoDB!'));


const userSchema = new mongoose.Schema({
	name: String,
	password: String
});

const User = mongoose.model('User', userSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.post('/signup', (req, res) => {
    User.findOne({ name: req.body.name }, (error, result) => {
        if (error) {
            return res.send(error)
        } else if (result != null && result.name == req.body.name) {
            return res.send('Duplicate user found!')
        } else {
            let newUser = new User({
                name: req.body.name,
                password: req.body.password
            })

            newUser.save((error, savedUser) => {
                if (error) {
                    return console.error(error)
                } else {
                    return res.status(201).send('New user Created!')
                }
            })
        }
    })
});


app.get('/users', (req, res) => {
	User.find({}, (error, result) => {
		if(error){
			return res.send(error)
		} else {
			return res.status(200).json({
				users: result
			})
		}
	})
});

app.listen(port, () => console.log(`Server is running at port ${port}`)); 